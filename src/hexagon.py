import math

class Hexagon:
    """Represents a 6-sided polygon"""
    
    def __init__(self, size):
        """Initializes the hexagon with a certain size"""
        self.size = size
        
    def height(self):
        return self.size * 2
        
    def width(self):
        return math.sqrt(3)/2 * self.height()
        
    def vert(self):
        """The vertical distance between two adjacent equally sized hexagons 
        so that they do not overlap
        
        """
        return 3./4 * self.height()