import numpy

class HexCoord:
    
    def __init__(self,coords):
        if len(coords) == 3:
            self._coords = numpy.array(coords)
        elif len(coords) == 2:
            self._coords = numpy.array(coords)
            self._coords.resize(3)
            self._coords[2] = -self.x() - self.z()
            
    @staticmethod
    def create_cube(x,y,z):
        """Creates new HexCoord with x, y, z cube coordinates"""
        return HexCoord([x,z,y])
        
    @staticmethod
    def create_axial(q,r):
        """Creates new HexCoord with q, r axial coordinates"""
        return HexCoord([q,r])
        
    def x(self):
        """x in cube coordinate system"""
        return self._coords[0]
        
    def z(self):
        """z in cube coordinate system"""
        return self._coords[1]
        
    def y(self):
        """y in cube coordinate system"""
        return self._coords[2]
        
    def q(self):
        """q in axial coordinate system"""
        return self.x()
        
    def r(self):
        """r in axial coordinate system"""
        return self.z()
        
    def cube(self):
        """returns cube coordinates"""
        return self._coords
        
    def axial(self):
        """returns axial coordinates"""
        return self._coords[:2]