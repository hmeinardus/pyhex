import unittest
import numpy
from src.hex_coord import HexCoord

class TestHexCoord(unittest.TestCase):
    
    def test_init_cube(self):
        c = HexCoord([1,-1,0])
        self.assertEqual(0, c.cube().sum())
        
    def test_init_axial(self):
        c = HexCoord([1,-2])
        self.assertEqual(0, c.cube().sum())
        self.assertEqual(2, len(c.axial()))
        self.assertEqual(3, len(c.cube()))
        
    def test_init_by_array(self):
        c1 = HexCoord(numpy.array([1,0]))
        c2 = HexCoord(numpy.array([1,0,-1]))
        self.assertTrue(numpy.array_equal(c1.cube(),c2.cube()))
        
    def test_create_cube(self):
        c = HexCoord.create_cube(1,0,-1)
        arr = numpy.array([1,-1,0])
        self.assertTrue(numpy.array_equal(arr,c.cube()))
        
    def test_create_axial(self):
        c = HexCoord.create_axial(1,0)
        arr = numpy.array([1,0,-1])
        self.assertTrue(numpy.array_equal(arr,c.cube()))