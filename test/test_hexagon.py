import unittest
import pdb
from src.hexagon import Hexagon

class TestHexagon(unittest.TestCase):
    
    def test_init_size(self):
        test_size = 5
        hex = Hexagon(test_size)
        self.assertEqual(test_size,hex.size)
        
    def test_init_docstring(self):
        self.assertIsNotNone(Hexagon.__init__.__doc__)
        
    def test_height_width_vert(self):
        test_size = 9
        hex = Hexagon(test_size)
        self.assertEqual(test_size*2,hex.height())
        self.assertLess(hex.width(),test_size*2)
        self.assertGreater(hex.width(),test_size*1.5)
        self.assertEqual(test_size*1.5,hex.vert())